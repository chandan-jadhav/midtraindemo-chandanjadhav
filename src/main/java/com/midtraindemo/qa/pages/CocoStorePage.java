package com.midtraindemo.qa.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.Reporter;

import com.midtraindemo.qa.base.TestBasePage;
import com.relevantcodes.extentreports.LogStatus;

public class CocoStorePage extends TestBasePage {
	
	// Initializing the Page objects
	
	public CocoStorePage(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	// Page Objects
	
	@FindBy(xpath="//a[@href='#/select-payment']")
	public WebElement continueButton;
	
	@FindBy(xpath="//iframe[@id='snap-midtrans']")
	public WebElement iframe;
	
	@FindBy(xpath="//div[text()='Credit Card']")
	public WebElement creditCardLink;
	
	@FindBy(xpath="//input[@name='cardnumber']")
	public WebElement cardNumberTextbox;
	
	@FindBy(xpath="//input[@placeholder='MM / YY']")
	public WebElement expiryDateTextbox;
	
	@FindBy(xpath="//input[@placeholder='123']")
	public WebElement cvvTextbox;
	
	@FindBy(xpath="//span[text()='Pay Now']")
	public WebElement payNowButton;
	
	@FindBy(xpath="//span[text()='Invalid card number']")
	public WebElement invalidCardNumberMessage;
	
	@FindBy(xpath="//span[text()='- Rp 2,000']//following-sibling::input")
	public WebElement selectPromoCode;
	
	@FindBy(xpath="//input[@id='PaRes']")
	public WebElement otpTextbox;
	
	@FindBy(xpath="//button[@name='ok']")
	public WebElement okButton;
	
	@FindBy(xpath="//div[text()='Transaction successful']")
	public WebElement transactionSucessfulMessage;
	
	
	// Actions
	
	public void verifyCocoStorePage() {
		driver.switchTo().frame(iframe);
		waitForElementToBeVisible(continueButton);
		if(continueButton.isDisplayed()){
			Reporter.log(LogStatus.PASS, "User is on Coco store page");
		} else {
			Reporter.log(LogStatus.FAIL, "User is unable to reach Coco store page");
		}
	}
	
	public void clickOnContinueButton(){
		waitForElementToBeVisible(continueButton);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", continueButton);    
		
	}
	
	public void clickOnCreditCardLink() {
		waitForElementToBeVisible(creditCardLink);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", creditCardLink);
	}
	
	public void enterCreditCardDetails(String cardnumber, String expiryDate, String cvvCode){
		
		cardNumberTextbox.sendKeys(cardnumber);
		expiryDateTextbox.sendKeys(expiryDate);
		cvvTextbox.sendKeys(cvvCode);
		
		
	}
	
	public void clickOnPayNowButton(){
		selectPromoCode.click();
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", payNowButton);
	}
	
	public void verifyInvalidCardNumber() {
		waitForElementToBeVisible(invalidCardNumberMessage);
		String invalidCardNumber = invalidCardNumberMessage.getText();
		if(invalidCardNumber.equalsIgnoreCase("Invalid card number")) {
			Reporter.log(LogStatus.PASS, "'Invalid card number' message is displayed sucessfully");
			
		} else {
			Reporter.log(LogStatus.FAIL,"Unable to display 'Invalid card number' message");
		}
	}
	
	public void enterOTP(String otp){
		waitForElementToBeVisible(otpTextbox);
		otpTextbox.sendKeys(otp);
		okButton.click();
	}
	
	public void verifyTransactionSucessfulMessage(){
		
		if(transactionSucessfulMessage.isDisplayed()){
			Reporter.log(LogStatus.PASS, "'Transaction sucessful' message is displayed sucessfully");
		} else {
			Reporter.log(LogStatus.FAIL, "Unable to display 'Transaction sucessful' message");
		}
		
	}
	
}
