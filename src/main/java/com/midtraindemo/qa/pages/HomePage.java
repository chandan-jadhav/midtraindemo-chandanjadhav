package com.midtraindemo.qa.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.Reporter;
import com.midtraindemo.qa.base.TestBasePage;
import com.relevantcodes.extentreports.LogStatus;

public class HomePage extends TestBasePage {
	
	// Initializing the Page objects
	
	public HomePage(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	// Page Objects
	
	@FindBy(xpath="//a[text()='BUY NOW']")
	public WebElement buyNowLink;
	
	// Actions
	
	public void verifyHomePage() {
		if(buyNowLink.isDisplayed()){
			Reporter.log(LogStatus.PASS, "User is on Home page");
		} else {
			Reporter.log(LogStatus.FAIL, "User is unable to reach Home page");
		}
	}
	
	public void clickOnBuyNowLink(){
		waitForElementToBeVisible(buyNowLink);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", buyNowLink);    
		
	}
	
	
	
}
