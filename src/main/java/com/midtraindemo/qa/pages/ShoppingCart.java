package com.midtraindemo.qa.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.midtraindemo.qa.base.TestBasePage;

public class ShoppingCart extends TestBasePage {
	
	// Initializing the Page objects
	
	public ShoppingCart(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	// Page Objects
	
	@FindBy(xpath="//div[text()='CHECKOUT']")
	public WebElement checkoutButton;

	
	// Actions
	
	public void clickOnCheckoutButton() {
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", checkoutButton);
	}
}
