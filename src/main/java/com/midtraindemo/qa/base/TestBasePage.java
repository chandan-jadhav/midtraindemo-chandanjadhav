package com.midtraindemo.qa.base;

import java.io.FileInputStream;
import java.lang.reflect.Method;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class TestBasePage {
	
	public static WebDriver driver;
	public static Properties prop;
	public static ExtentTest Reporter;
	public static ExtentReports report;
	
	public void waitForElementToBeVisible(WebElement element){
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	
	@BeforeTest
	public void beforeTest(){
		report = new ExtentReports("./Reports/ExtentReport.html");
	}
	
	@BeforeMethod
	public void setUp(Method method) {
		try {
			
			Reporter = report.startTest(method.getName());
			
			prop = new Properties();
			FileInputStream fis = new FileInputStream("./src/main/java/com/midtraindemo/qa/"
					+ "config/config.properties");
			prop.load(fis);
			
		} catch(Exception e){
			e.printStackTrace();
		}
		
		String browserName = prop.getProperty("browserName");
		if (browserName.equalsIgnoreCase("chrome")){
			System.setProperty("webdriver.chrome.driver", "./Resources/chromedriver.exe");
			driver = new ChromeDriver();
		} else if (browserName.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "./Resources/geckodriver.exe");
			driver = new FirefoxDriver();
		}
		
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(prop.getProperty("baseUrl"));
		
	}
	
	@AfterMethod
	public void tearDown() {
		driver.close();
		report.endTest(Reporter);
		report.flush();
	}
	
}
