package com.traindemo.qa.testcases;


import org.testng.annotations.Test;
import com.midtraindemo.qa.base.TestBasePage;
import com.midtraindemo.qa.pages.CocoStorePage;
import com.midtraindemo.qa.pages.HomePage;
import com.midtraindemo.qa.pages.ShoppingCart;

public class PurchasePillowTestCases extends TestBasePage {
	
	@Test(description="Purchase Pillow using valid Credit card number")
	public void purchasePillowUsingValidCreditCard() {
		
		String cardNumber = prop.getProperty("validCardNumber");
		String expiryDate = prop.getProperty("expiryDate");
		String cvvCode = prop.getProperty("cvvCode");
		String otp = prop.getProperty("otp");
		
		HomePage homePage = new HomePage(driver);
		ShoppingCart shoppingCart = new ShoppingCart(driver);
		CocoStorePage cocoStorePage = new CocoStorePage(driver);
		
		homePage.verifyHomePage();
		homePage.clickOnBuyNowLink();
		shoppingCart.clickOnCheckoutButton();
		cocoStorePage.verifyCocoStorePage();
		cocoStorePage.clickOnContinueButton();
		cocoStorePage.clickOnCreditCardLink();
		cocoStorePage.enterCreditCardDetails(cardNumber, expiryDate, cvvCode);
		cocoStorePage.clickOnPayNowButton();
		cocoStorePage.enterOTP(otp);
		cocoStorePage.verifyTransactionSucessfulMessage();
	}
	
	@Test(description="Purchase Pillow using invalid Credit card number")
	public void purchasePillowUsingInvalidCreditCard() {
		
		String cardNumber = prop.getProperty("invalidCardNumber");
		String expiryDate = prop.getProperty("expiryDate");
		String cvvCode = prop.getProperty("cvvCode");
		
		HomePage homePage = new HomePage(driver);
		ShoppingCart shoppingCart = new ShoppingCart(driver);
		CocoStorePage cocoStorePage = new CocoStorePage(driver);
		
		homePage.verifyHomePage();
		homePage.clickOnBuyNowLink();
		shoppingCart.clickOnCheckoutButton();
		cocoStorePage.verifyCocoStorePage();
		cocoStorePage.clickOnContinueButton();
		cocoStorePage.clickOnCreditCardLink();
		cocoStorePage.enterCreditCardDetails(cardNumber, expiryDate, cvvCode);
		cocoStorePage.clickOnPayNowButton();
		cocoStorePage.verifyInvalidCardNumber();
	}

}
